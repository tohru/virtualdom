function Idris$Elma$VirtualDom$putInQueue(value, queue){
  if(queue.callback == undefined){
    queue.queue.push(value);
  }else{
    var callB = queue.callback;
    queue.callback = undefined;
    callB(value);
  }
}

function Idris$Elma$VirtualDom$getFromQueue(queue, callback){
  if(queue.queue.length > 0){
    callback(queue.queue.shift());
  }else{
    queue.callback = callback;
  }
}

function Idris$Elma$VirtualDom$_processEvent(x, i, e) {
  if (x.attrs[i] !== undefined && x.attrs[i] !== null) {
    Idris$Elma$VirtualDom$putInQueue(x.attrs[i].read(e), x.ctx.queue);
  }
}

function Idris$Elma$VirtualDom$_initAttribute(node, x, i) {
  switch (x.attrs[i].type) {
    case 'r':
      node.setAttribute(x.attrs[i].name, x.attrs[i].value);
      break;
    case 'rn':
      node.setAttributeNS(x.attrs[i].ns, x.attrs[i].n, x.attrs[i].value);
      break;
    case 'e':
      node.addEventListener(x.attrs[i].name, function (e) { Idris$Elma$VirtualDom$_processEvent(x, i, e) });
      break;
    case 'ec':
      x.attrs[i].init({ domNode: node, process: function (e) { Idris$Elma$VirtualDom$_processEvent(x, i, e) } });
      break;
    case 'ep':
      node.addEventListener(x.attrs[i].name, function (e) { e.preventDefault(); Idris$Elma$VirtualDom$_processEvent(x, i, e) });
      break;
    case 'p':
      node[x.attrs[i].name] = x.attrs[i].value;
      break;
    case 's':
      for (var j in x.attrs[i].value) {
        node.style[j] = x.attrs[i].value[j];
      }
      break;
    case 'b':
      node[x.attrs[i].name] = x.attrs[i].value != 0
      break;
  }
}

function Idris$Elma$VirtualDom$_initChildsAndAttributes(x) {
  for (let i in x.attrs) {
    Idris$Elma$VirtualDom$_initAttribute(x.domNode, x, i);
  }
  for (var i = 0; i < x.childs.length; ++i) {
    x.childs[i].parent = x.domNode;
    Idris$Elma$VirtualDom$_initialyze(x.ctx, x.childs[i]);
  }
}

function Idris$Elma$VirtualDom$_initialyze(ctx, x) {
  x.ctx = ctx;
  switch (x.type) {
    case 'n':
      var node = document.createElement(x.tag);
      x.domNode = node;
      x.parent.appendChild(node);
      Idris$Elma$VirtualDom$_initChildsAndAttributes(x);
      break;
    case 'nn':
      var node = document.createElementNS(x.ns, x.tag);
      x.domNode = node;
      x.parent.appendChild(node);
      Idris$Elma$VirtualDom$_initChildsAndAttributes(x);
      break;
    case 't':
      var t = document.createTextNode(x.text);
      x.parent.appendChild(t);
      x.domNode = t;
      break;
  }
}

function Idris$Elma$VirtualDom$_replaceNode(x, y) {
  y.ctx = x.ctx;
  switch (y.type) {
    case 'n':
      var node = document.createElement(y.tag);
      x.parent.replaceChild(node, x.domNode);
      y.parent = x.parent;
      y.domNode = node;
      Idris$Elma$VirtualDom$_initChildsAndAttributes(y);
      break;
    case 'nn':
      var node = document.createElementNS(y.ns, y.tag);
      x.parent.replaceChild(node, x.domNode);
      y.parent = x.parent;
      y.domNode = node;
      Idris$Elma$VirtualDom$_initChildsAndAttributes(y);
      break;
    case 't':
      var node = document.createTextNode(y.text);
      x.parent.replaceChild(node, x.domNode);
      y.parent = x.parent;
      y.domNode = node;
      break;
  }
  for (var member in x) delete x[member];
  for (var member in y) x[member] = y[member];
}

function Idris$Elma$VirtualDom$attributesListToObj(x) {
  var res = {};
  for (var i = x.length - 1; i >= 0; --i) {
    if (x[i].type === 's') {
      if (res[x[i].name] === undefined) {
        res[x[i].name] = x[i];
      } else {
        Object.assign(res[x[i].name].value, x[i].value)
      }
    } else {
      res[x[i].name] = x[i];
    }
  }
  return res;
}

function Idris$Elma$VirtualDom$_updateStyle(node, xstl, ystl) {
  var i;
  for (i in ystl) {
    if (xstl[i] !== ystl[i]) {
      node.style[i] = ystl[i];
    }
  }
  for (i in xstl) {
    if (ystl[i] === undefined) {
      node.style[i] = "";
    }
  }
}

function Idris$Elma$VirtualDom$_stylesEq(xstl, ystl) {
  var i;
  for (i in ystl) {
    if (xstl[i] !== ystl[i]) {
      return false;
    }
  }
  for (i in xstl) {
    if (ystl[i] !== undefined) {
      return false;
    }
  }
  return true;
}

function Idris$Elma$VirtualDom$_calcStyleFramesAndDoUpdates(node, xstl, ystl) {
  var r1 = {};
  var r2 = {};
  var i;
  for (i in ystl) {
    if (xstl[i] !== ystl[i]) {
      if (xstl[i] !== undefined) {
        r1[i] = xstl[i];
        r2[i] = ystl[i];
      } else {
        node.style[i] = ystl[i]
      }
    }
  }
  for (i in xstl) {
    if (ystl[i] === undefined) {
      node.style[i] = "";
    }
  }
  return [r1, r2];
}

function Idris$Elma$VirtualDom$_updateAttributes(animations, x, y) {
  for (var i in y.attrs) {
    if (y.attrs[i].type[0] === 'e') {
      if (x.attrs[i] === undefined) {
        x.attrs[i] = y.attrs[i];
        Idris$Elma$VirtualDom$_initAttribute(x.domNode, x, i);
      }
    } else if (x.attrs[i].value !== y.attrs[i].value) {
      switch (x.attrs[i].type) {
        case 'r':
          x.domNode.setAttribute(y.attrs[i].name, y.attrs[i].value);
          break;
        case 'rn':
          x.domNode.setAttributeNS(y.attrs[i].ns, y.attrs[i].n, y.attrs[i].value);
          break;
        case 'p':
          x.domNode[y.attrs[i].name] = y.attrs[i].value
          break;
        case 's':
          if (!Idris$Elma$VirtualDom$_stylesEq(x.attrs[i].value, y.attrs[i].value)) {
            if (animations !== undefined) {
              let keyframes = Idris$Elma$VirtualDom$_calcStyleFramesAndDoUpdates(x.domNode, x.attrs[i].value, y.attrs[i].value)
              if (Object.keys(keyframes).length > 0) {
                x.domNode.animate(keyframes, animations);
              }
            } else {
              Idris$Elma$VirtualDom$_updateStyle(x.domNode, x.attrs[i].value, y.attrs[i].value);
            }
          }
          break;
        case 'b':
          x.domNode[y.attrs[i].name] = y.attrs[i].value != 0

      }
    }
  }
  for (var i in x.attrs) {
    if (y.attrs[i] === undefined && x.attrs[i] !== null) {
      if (x.attrs[i].type[0] === 'e') {
        y.attrs[i] = null;
      } else {
        x.domNode.removeAttribute(i);
      }
    }
  }
  x.attrs = y.attrs;
}

function Idris$Elma$VirtualDom$_updateChilds(animations, x, y) {
  var i = 0;
  for (; i < Math.min(x.childs.length, y.childs.length); ++i) {
    Idris$Elma$VirtualDom$_update(animations, x.childs[i], y.childs[i]);
  }
  for (; i < y.childs.length; ++i) {
    y.childs[i].parent = x.domNode;
    Idris$Elma$VirtualDom$_initialyze(x.ctx, y.childs[i]);
    x.childs.push(y.childs[i]);
  }
  for (; i < x.childs.length; ++i) {
    x.domNode.removeChild(x.childs[i].domNode);
  }
  x.childs.splice(y.childs.length);
}

function Idris$Elma$VirtualDom$_update(animations, x, y) {
  if (x.type === 'n' && y.type === 'n' && x.tag === y.tag) {
    Idris$Elma$VirtualDom$_updateAttributes(animations, x, y);
    Idris$Elma$VirtualDom$_updateChilds(animations, x, y);
  } else if (x.type === 'nn' && y.type === 'nn' && x.ns === y.ns && x.tag === y.tag) {
    Idris$Elma$VirtualDom$_updateAttributes(animations, x, y);
    Idris$Elma$VirtualDom$_updateChilds(animations, x, y);
  } else if (x.type === 't' && y.type === 't' && y.text === x.text) {
    ;
  } else {
    Idris$Elma$VirtualDom$_replaceNode(x, y);
  }
}

function Idris$Elma$VirtualDom$update(x, y) {
  y.ctx = x.ctx;
  Idris$Elma$VirtualDom$_update(undefined, x, y)
}

function Idris$Elma$VirtualDom$animate(animations, x, y) {
  y.ctx = x.ctx;
  Idris$Elma$VirtualDom$_update(animations, x, y)
}

function Idris$Elma$VirtualDom$updateQueue(queue, x) {
  x.queue = queue;
}

function Idris$Elma$VirtualDom$initialyzeBody(queue, x) {
  x.parent = document.body;
  Idris$Elma$VirtualDom$_initialyze({queue: queue}, x)
}

function Idris$Elma$VirtualDom$clear(x) {
  x.parent.removeChild(x.domNode);
}

function Idris$Elma$VirtualDom$mkStyle(x, y) {
  var res = {};
  res[x] = y
  return res;
}

function Idris$Elma$VirtualDom$availableWidth() {
  var stl = window.getComputedStyle(document.body);
  return window.innerWidth - parseInt(stl.marginLeft) - parseInt(stl.marginRight) - parseInt(stl.paddingLeft) - parseInt(stl.paddingRight)
}

function Idris$Elma$VirtualDom$availableHeight() {
  var stl = window.getComputedStyle(document.body);
  return window.innerHeight - parseInt(stl.marginTop) - parseInt(stl.marginBottom) - parseInt(stl.paddingTop) - parseInt(stl.paddingBottom)
}

function Idris$Elma$VirtualDom$mapAttribute(f, attr) {
	if (attr.type[0] === 'e') {
		var res = {};
		Object.assign(res, attr);
		res.read = (function(x){ return f(attr.read(x)) });
		return res;
	} else {
		return attr
	}
}

function Idris$Elma$VirtualDom$mapNode(f, node) {
	var res = {};
	Object.assign(res, node);
	for (var x in res.attrs) {
		res.attrs[x] = Idris$Elma$VirtualDom$mapAttribute(f, node.attrs[x]);
	}
	for (var x in res.childs) {
		res.childs[x] = Idris$Elma$VirtualDom$mapNode(f, node.childs[x]);
	}
	return res;
}

// Exported:
// - initialyzeBody
// - update
// - animate
// - updateQueue
// - clear
// - attributesListToObj
// - mkStyle
// - availableWidth
// - availableHeight
// - mapAttribute
// - mapNode
