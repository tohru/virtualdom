module Elma.VirtualDom

import Elma.VirtualDom.Utils

%include JavaScript "VirtualDom.js"

%access export

public export
data JSIOFifoQueue : (b : Type) -> Type where
  MkJSIOFifoQueue : Ptr -> JSIOFifoQueue a

%name JSIOFifoQueue queue


newJSIOFifoQueue : (x : Type) -> JS_IO (JSIOFifoQueue x)
newJSIOFifoQueue _ =
  MkJSIOFifoQueue <$> jscall "{queue: [], callback: undefined}" (JS_IO Ptr)


putInQueue : a -> JSIOFifoQueue a -> JS_IO ()
putInQueue x (MkJSIOFifoQueue q) =
  jscall
    "Idris$Elma$VirtualDom$putInQueue(%0,%1)"
    (Ptr -> Ptr -> JS_IO ())
    (believe_me x)
    q


getQueuePtr : JSIOFifoQueue a -> Ptr
getQueuePtr (MkJSIOFifoQueue q) = q


data Node : Type -> Type -> Type where
  MkNode : Ptr -> Node a b


Functor (Node a) where
  map f (MkNode x) =
    assert_total $ MkNode $ unsafePerformIO $
      jscall "Idris$Elma$VirtualDom$mapNode(%0, %1)"
      (JsFn (Ptr -> Ptr) -> Ptr -> JS_IO Ptr)
      (MkJsFn $ believe_me f)
      x


data Attribute : Type -> Type -> Type where
  MkAttribute : Ptr -> Attribute a b


Functor (Attribute a) where
  map f (MkAttribute x) =
    assert_total $ MkAttribute $ unsafePerformIO $
      jscall
        "Idris$Elma$VirtualDom$mapAttribute(%0, %1)"
        (JsFn (Ptr -> Ptr) -> Ptr -> JS_IO Ptr)
        (MkJsFn $ believe_me f)
        x


node : String -> List (Attribute b a) -> List (Node c a) -> Node c a
node tag attrs childs =
  let attrsPtr =
      unsafePerformIO $
        (makeJSList $ map (\(MkAttribute x) => x) attrs) >>=
          jscall "Idris$Elma$VirtualDom$attributesListToObj(%0)" (Ptr -> JS_IO Ptr)
      childsPtr = unsafePerformIO $ makeJSList $ map (\(MkNode x) => x) childs
  in MkNode $ unsafePerformIO $ jscall "{type: 'n', tag: %0, attrs: %1, childs: %2}" (String -> Ptr -> Ptr -> JS_IO Ptr) tag attrsPtr childsPtr


nodeNS : String -> String -> List (Attribute b a) -> List (Node c a) -> Node c a
nodeNS ns tag attrs childs =
  let attrsPtr =
      unsafePerformIO $
        (makeJSList $ map (\(MkAttribute x) => x) attrs) >>=
          jscall "Idris$Elma$VirtualDom$attributesListToObj(%0)" (Ptr -> JS_IO Ptr)
      childsPtr = unsafePerformIO $ makeJSList $ map (\(MkNode x) => x) childs
  in MkNode $ unsafePerformIO $ jscall "{type: 'nn', ns: %0 , tag: %1, attrs: %2, childs: %3}" (String -> String -> Ptr -> Ptr -> JS_IO Ptr) ns tag attrsPtr childsPtr


text : String -> Node c a
text txt =
  MkNode $ unsafePerformIO $ jscall "{type: 't', text: %0}" (String -> JS_IO Ptr) txt


attribute : String -> String -> Attribute a b
attribute name value =
  MkAttribute $ unsafePerformIO $
    jscall
      "{type:'r',name:%0, value:%1}"
      (String -> String -> JS_IO Ptr)
      name
      value


attributeNS : String -> String -> String -> Attribute a b
attributeNS ns name value =
  MkAttribute $ unsafePerformIO $
    jscall
      "{type:'rn', ns: %0, n:%1, name:%0+':'+%1, value:%2}"
      (String -> String -> String -> JS_IO Ptr)
      ns
      name
      value


stringProperty : String -> String -> Attribute a b
stringProperty name value =
  MkAttribute $ unsafePerformIO $
    jscall
      "{type:'p', name:%0, value:%1}"
      (String -> String -> JS_IO Ptr)
      name
      value


booleanProperty : String -> Bool -> Attribute a b
booleanProperty name value = 
  MkAttribute $ unsafePerformIO $
    jscall
      "{type:'b', name:%0, value:%1}"
      (String -> Int -> JS_IO Ptr)
      name
      (if value then 1 else 0)


data Style = MkStyle Ptr


mkStyle : String -> String -> Style
mkStyle n v =
  MkStyle $ unsafePerformIO $
    jscall
      "Idris$Elma$VirtualDom$mkStyle(%0,%1)"
      (String -> String -> JS_IO Ptr)
      n
      v


Nil : Style
Nil =
  MkStyle $ unsafePerformIO $
    jscall
      "{}"
      (JS_IO Ptr)


(::) : Style -> Style -> Style
(::) (MkStyle x) (MkStyle y) =
  MkStyle $ unsafePerformIO $
    jscall
      "Object.assign(%0,%1)"
      (Ptr -> Ptr -> JS_IO Ptr)
      y
      x


style : Style -> Attribute a b
style (MkStyle stl) =
  MkAttribute $ unsafePerformIO $
    jscall
      "{type:'s', name: 'style', value:%0}"
      (Ptr -> JS_IO Ptr)
      stl


eventListenerAttribute : String -> (Ptr -> JS_IO b) -> Attribute a b
eventListenerAttribute name read =
  MkAttribute $ unsafePerformIO $
    jscall
      "{type:'e', name:%0, read: %1}"
      (String -> JsFn (Ptr -> JS_IO Ptr) -> JS_IO Ptr)
      name
      (MkJsFn $ believe_me read)


customEventListenerAttribute : String -> (Ptr -> JS_IO ()) -> (Ptr -> JS_IO b) -> Attribute a b
customEventListenerAttribute name initE read =
  MkAttribute $ unsafePerformIO $
    jscall
      "{type:'ec', name:%0, init: %1 , read: %2}"
      (String -> JsFn (Ptr -> JS_IO ()) -> JsFn (Ptr -> JS_IO Ptr) -> JS_IO Ptr)
      name
      (MkJsFn $ initE)
      (MkJsFn $ believe_me read)


eventListenerAttributePreventDefault : String -> (Ptr -> JS_IO b) -> Attribute a b
eventListenerAttributePreventDefault event convert =
  MkAttribute $ unsafePerformIO $
    jscall
      "{type:'ep', name:%0, read: %1}"
      (String -> JsFn (Ptr -> JS_IO Ptr) -> JS_IO Ptr)
      event
      (MkJsFn $ believe_me convert)


initialyzeBody : JSIOFifoQueue b -> Node a b -> JS_IO ()
initialyzeBody q (MkNode x) =
  jscall
   "Idris$Elma$VirtualDom$initialyzeBody(%0, %1)"
   (Ptr -> Ptr -> JS_IO ())
   (getQueuePtr q)
   x


updateNode : Node a b -> Node a b -> JS_IO ()
updateNode (MkNode x) (MkNode y) =
  jscall
    "Idris$Elma$VirtualDom$update(%0,%1)"
    (Ptr -> Ptr -> JS_IO ())
    x
    y


updateNodeM : JSIOFifoQueue b -> Node a c -> Node a b -> JS_IO (Node a b)
updateNodeM q (MkNode x) (MkNode y) =
  do
    jscall
      "Idris$Elma$VirtualDom$updateQueue(%0, %1)"
      (Ptr -> Ptr -> JS_IO ())
      (getQueuePtr q)
      x
    jscall
      "Idris$Elma$VirtualDom$update(%0,%1)"
      (Ptr -> Ptr -> JS_IO ())
      x
      y
    pure $ MkNode x


clearNode : Node a b -> JS_IO ()
clearNode (MkNode x) = jscall "Idris$Elma$VirtualDom$clear(%0)" (Ptr -> JS_IO ()) x


public export
data AnimationOption : Type where
  Duration : Nat -> AnimationOption

readAnimationOptions : List AnimationOption -> JS_IO Ptr
readAnimationOptions [] =
  jscall "{'fill':'both'}" (JS_IO Ptr)
readAnimationOptions ((Duration d)::r) =
  readAnimationOptions r >>= \x => jscall "Object.assign(%0,{'duration':%1})" (Ptr -> Int -> JS_IO Ptr) x (cast d)


animateNode : List AnimationOption -> Node a b -> Node a b -> JS_IO ()
animateNode animOpts (MkNode x) (MkNode y) =
  do
    opts <- readAnimationOptions animOpts
    jscall
      "Idris$Elma$VirtualDom$animate(%0,%1,%2)"
      (Ptr -> Ptr -> Ptr -> JS_IO ())
      opts
      x
      y


duration : Nat -> AnimationOption
duration = Duration


availableHeight_ : JS_IO Double
availableHeight_ = jscall "Idris$Elma$VirtualDom$availableHeight()" (JS_IO Double)


availableWidth_ : JS_IO Double
availableWidth_ = jscall "Idris$Elma$VirtualDom$availableWidth()" (JS_IO Double)
